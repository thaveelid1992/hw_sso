const passport = require("passport");
//ใช้ในการ decode jwt ออกมา
const ExtractJwt = require("passport-jwt").ExtractJwt;
//ใช้ในการประกาศ Strategy
const JwtStrategy = require("passport-jwt").Strategy;
const FacebookStrategy = require('passport-facebook').Strategy;

const SECRET = "cac56cce811dfe63c9a461154b3815ec";

var moment = require('moment-timezone');
var moment = require('moment');

var us = require('underscore');

const notcolumn = {user_password : false,user_status_by:false,user_create_date:false,user_create_by:false,user_update_date:false,user_update_by:false};

//////// db //////////
var mongodb = require('../config/db');
var mongoOb = require('mongojs');
var db = mongodb.connect;
//////////////////////

var us = require('underscore');

var CLIENT_ID = '704209800010458';
var CLIENT_SECRET = '31ec053dcd9e42d10ef4997666af71bf';

passport.serializeUser(function(user, done) {
  done(null, user); //อยากส่งอะไรไปเก็บใน session
})
passport.deserializeUser(function(obj, done) {
  done(null, obj); //เอาของที่เก็บใน session มาใช้ต่อ
})

//สร้าง Strategy
const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromHeader("authorization"),
  secretOrKey: SECRET
};

const jwtAuth = new JwtStrategy(jwtOptions, (payload, done) => {
  db.SSO_USER.findOne({user_name : payload.user_name},notcolumn, function (err, saved) {
    if (err) {
      done(err);
    } else {
        if(saved){
          done(null, saved);
        }else{
          done(null, false,{ msg: `Username Email ${payload.user_name} not found.` });
        }
    }
});
});

passport.use(jwtAuth);

passport.use(new FacebookStrategy({
  clientID: CLIENT_ID,
  clientSecret: CLIENT_SECRET,
  callbackURL: "http://localhost:3000/auth/facebook/callback",
  profileFields: ['name', 'email', 'link', 'locale', 'timezone', 'gender'],
},
function(accessToken, refreshToken, profile, done) {
  console.log(profile);
  console.log('accessToken : ' + accessToken );
  var body = {};
  body.id_facebook = profile.id;
  body.user_first_name = profile._json.first_name;
  body.user_last_name = profile._json.last_name;
  body.user_accessToken = accessToken;
  body.user_email = profile._json.email;
  body.user_gender = profile.gender;
  body.user_profileurl = profile.profileUrl;
  body.user_status_by = 'Joe Dev';
  body.user_create_date = moment().tz('Asia/Bangkok').toISOString();
  body.user_create_by = 'Joe Dev';
  body.user_update_date = moment().tz('Asia/Bangkok').toISOString();
  body.user_update_by = 'Joe Dev';
  if(profile){
    db.SSO_USER.findOne({user_email : profile._json.email},notcolumn, function (err, saved) {
      if (err) {
        done(err);
      } else {
          if(saved){
            done(null, saved);
          }else{
            db.SSO_USER.insert(body, function (err, saved) {
              if (err) {
                  // res.status(500).json({success: false ,statuscode : 500 ,message : 'Not Connect To Database'});
                  done(err);
              } else {
                // done(null, false,{ msg: `Username Email ${profile._json.email} not found.` });
                done(null, body);
                // res.status(200).json({success: true, status_code : 200, data: body});
              }
          });
          }
      }
  });
  }
}
));



