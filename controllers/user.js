const passport = require('passport');
const jwt = require("jwt-simple");
var mongodb = require('../config/db');
var mongoOb = require('mongojs');
var ObjectId = require('mongodb').ObjectId; 
var us = require('underscore');
var moment = require('moment-timezone');
var moment = require('moment');
var nodemailer = require('nodemailer');
var db = mongodb.connect;

const  SECRET = "cac56cce811dfe63c9a461154b3815ec";

var datenow = moment().tz('Asia/Bangkok').toISOString();

////// hash password /////////
const bcrypt = require('bcrypt');
const saltRounds = 10;
//////////////////////////////

////////// Middleware /////////////
exports.loginMiddleware = (req, res, next) => {
    var bodyObject = us.pick(req.body, 'user_name','user_password');
    db.SSO_USER.findOne({user_name : bodyObject.user_name}, function (err, saved) {
        if (err) {
            res.status(500).json({success: false ,statuscode : 500 ,message : 'Not Connect To Database'});
        } else {
            if(saved){
              comparePassword(bodyObject.user_password,saved.user_password).then(() => {
                  res.locals.UserObject = saved;
                  next();
                  }).catch((err)=>{
                    res.status(500).json({success: false ,statuscode : 500 ,message : 'Password Invaild '});
                  }); 
            }else{
              res.status(500).json({success: false ,statuscode : 500 ,message : 'Wrong username'}); 
            }
        }
    });
 };
////////// End Middleware /////////////


exports.saveUserRegister = (req, res, next) => {
  var bodyObject = us.pick(req.body, 'user_name','user_password','user_first_name','user_last_name','user_status','user_tel','user_email','user_nickname','user_birthday');
  var body = {};
  db.SSO_USER.findOne({user_name : bodyObject.user_name}, function (err, saved) {
    if (err) {
        res.status(500).json({success: false ,statuscode : 500 ,message : 'Not Connect To Database'});
    } else {
        if(saved){
          res.status(500).json({success: false ,statuscode : 500 ,message : 'Username already exists'});   
        }else{
            hashPassword(bodyObject.user_password).then((hashPassword) => {
                if(bodyObject.user_name)body.user_name = bodyObject.user_name;
                if(hashPassword)body.user_password = hashPassword;
                if(bodyObject.user_first_name)body.user_first_name = bodyObject.user_first_name;
                if(bodyObject.user_last_name)body.user_last_name = bodyObject.user_last_name;
                if(bodyObject.user_status)body.user_status = bodyObject.user_status;
                if(bodyObject.user_tel)body.user_tel = bodyObject.user_tel;
                if(bodyObject.user_email)body.user_email = bodyObject.user_email;
                if(bodyObject.user_nickname)body.user_nickname = bodyObject.user_nickname;
                if(bodyObject.user_birthday)body.user_birthday = bodyObject.user_birthday;
                var user_fullname = bodyObject.user_first_name + ' ' + bodyObject.user_last_name;
                body.user_isactive = 1;
                body.user_status_by = user_fullname;
                body.user_create_date = datenow;
                body.user_create_by = user_fullname;
                body.user_update_date = datenow;
                body.user_update_by = user_fullname;
                db.SSO_USER.insert(body, function (err, saved) {
                  if (err) {
                      res.status(500).json({success: false ,statuscode : 500 ,message : 'Not Connect To Database'});
                  } else {
                      res.status(200).json({success: true, status_code : 200, data: body});
                  }
              });
                }).catch((err)=>{
                  res.status(500).json({success: false ,statuscode : 500 ,message : 'Password Hash failed'});
                });
        }
    }
});
};

exports.saveUserRegisterCompany = (req, res, next) => {
  var bodyObject = us.pick(req.body, 'company_firstname','company_lastname','company_email','company_phone','company_name','company_package_id');
  var body = {};
  var bodyUser = {};
  var user_fullname = bodyObject.company_firstname + ' ' + bodyObject.company_firstname;
  if(bodyObject.company_email)body.company_email = bodyObject.company_email;
  if(bodyObject.company_phone)body.company_phone = bodyObject.company_phone;
  if(bodyObject.company_name)body.company_name = bodyObject.company_name;
  if(bodyObject.company_package_id)body.company_package_id = new ObjectId(bodyObject.company_package_id);
  body.company_isactive = 1;
  body.company_status_by = user_fullname;
  body.company_create_date = datenow;
  body.company_create_by = user_fullname;
  body.company_update_date = datenow;
  body.company_update_by = user_fullname;
  db.SSO_PACKAGE_SERVICE.findOne({_id : body.company_package_id}, function (err, saved) {
    if (err) {
        res.status(500).json({success: false ,statuscode : 500 ,message : 'Not Connect To Database'});
    } else {
        if(saved){
           db.SSO_COMPANY.findOne({company_email : bodyObject.company_email}, function (err, saved) {
            if (err) {
                res.status(500).json({success: false ,statuscode : 500 ,message : 'Not Connect To Database'});
            } else {
                if(saved){
                    res.status(200).json({success: false ,statuscode : 200 ,message : `${bodyObject.company_email} already exists`});
                }else{
                    db.SSO_COMPANY.insert(body, function (err, saved) {
                        if (err) {
                            res.status(500).json({success: false ,statuscode : 500 ,message : 'Not Connect To Database'});
                        } else {
                            hashPassword('happywork').then((hashPassword) => {
                                if(bodyObject.company_firstname)bodyUser.user_first_name = bodyObject.company_firstname;
                                if(bodyObject.company_lastname)bodyUser.user_last_name = bodyObject.company_lastname;
                                if(bodyObject.company_phone)bodyUser.user_tel = bodyObject.company_phone;
                                if(bodyObject.company_email){
                                    bodyUser.user_email = bodyObject.company_email;
                                    bodyUser.user_name = bodyObject.company_email;
                                }
                                if(hashPassword)bodyUser.user_password = hashPassword;
                                if(body._id)bodyUser.user_company_id = new ObjectId(body._id);
                                bodyUser.user_status = 1;
                                bodyUser.user_status_by = user_fullname;
                                bodyUser.user_create_date = datenow;
                                bodyUser.user_create_by = user_fullname;
                                bodyUser.user_update_date = datenow;
                                bodyUser.user_update_by = user_fullname;
                                db.SSO_USER.insert(bodyUser, function (err, saved) {
                                    if (err) {
                                        res.status(500).json({success: false ,statuscode : 500 ,message : 'Not Connect To Database'});
                                    } else {
                                        // res.status(200).json({success: true, status_code : 200, message : 'Insert Company and User is Suscess',datacompany: body, datauser : bodyUser});
                                        res.status(200).json({success: true, status_code : 200, message : 'Insert Company and User is Suscess'});
                                    }
                                });
                                }).catch((err)=>{
                                  res.status(500).json({success: false ,statuscode : 500 ,message : 'Password Hash failed'});
                                });
                        }
                    });
                }
            }
        }); 
        }else{
            res.status(200).json({success: false ,statuscode : 200 ,message : 'Package not found.'});
        }
    }
});
};

exports.signUp = (req, res, next) => {
    var bodyObject = us.pick(req.body, 'user_name','user_password');
    const payload = {
        user_name: bodyObject.user_name,
        user_first_name : res.locals.UserObject.user_first_name,
        user_last_name : res.locals.UserObject.user_last_name,
        user_status : res.locals.UserObject.user_status,
        user_tel : res.locals.UserObject.user_tel,
        user_email : res.locals.UserObject.user_email,
        user_nickname : res.locals.UserObject.user_nickname,
        user_birthday : res.locals.UserObject.user_birthday,
        iat: new Date().getTime()//มาจากคำว่า issued at time (สร้างเมื่อ)
     };
     //ในการใช้งานจริง คีย์นี้ให้เก็บเป็นความลับ
     res.status(200).json({success: true, status_code : 200, token: jwt.encode(payload, SECRET),data : payload});
    //  res.send(jwt.encode(payload, SECRET));
};

exports.account = (req, res, next) => {
    res.status(200).json({success: true, status_code : 200, data: req.user});
};

exports.updateCompany = (req, res, next) => {
    var bodyObject = us.pick(req.body, 'company_id','company_name_th','company_name_en','company_address','company_phone','company_tax_id','company_package_member','company_suffix_email','company_working_hours','company_isactive');
    var body = {};
    if(bodyObject.company_name_th)body.company_name_th = bodyObject.company_name_th;
    if(bodyObject.company_name_en)body.company_name_en = bodyObject.company_name_en;
    if(bodyObject.company_address)body.company_address = bodyObject.company_address;
    if(bodyObject.company_phone)body.company_phone = bodyObject.company_phone;
    if(bodyObject.company_tax_id)body.company_tax_id = bodyObject.company_tax_id ;
    if(bodyObject.company_package_member)body.company_package_member = bodyObject.company_package_member;
    if(bodyObject.company_suffix_email)body.company_suffix_email = bodyObject.company_suffix_email;
    if(bodyObject.company_working_hours)body.company_working_hours = bodyObject.company_working_hours ;
    if(bodyObject.company_isactive)body.company_isactive = bodyObject.company_isactive;
    body.company_update_date = datenow;
    body.company_update_by = req.user.user_first_name + ' ' + req.user.user_last_name;
    db.SSO_COMPANY.update({_id:new ObjectId(bodyObject.company_id)},{$set:body},
        function(err, obj) {
            if(err){
                res.status(500).json({success: false ,statuscode : 500 ,message : 'Not Connect To Database'});
            }else{
                if(obj.nModified > 0){
                    res.status(200).json({success: true, status_code : 200, message : 'Insert Company and User is Suscess'});
                }else{
                    res.status(200).json({success: false ,statuscode : 200 ,message : `${bodyObject.company_name_th} already exists`});
                }
            }
        }
    );
};

exports.getForgot = (req, res) => {
    res.render('forgot');
};

exports.postForgot = (req, res, next) => {
    console.log(req.body);
    setSendMail(res);
};

function getNextSequenceValue(sequenceName){
    var sequenceDocument = db.SSO_USER.findAndModify({
       query:{_id: sequenceName },
       update: {$inc:{seq:1}},
       new:true
    });
    return sequenceDocument.seq;
 }

 function setSendMail(res){
     return new Promise((resolve, reject) => {
        nodemailer.createTestAccount((err, account) => {
            if (err) {
                console.error('Failed to create a testing account. ' + err.message);
                return process.exit(1);
            }
        
            console.log('Credentials obtained, sending message...');
        
            // Create a SMTP transporter object
            let transporter = nodemailer.createTransport({
                host: account.smtp.host,
                port: account.smtp.port,
                secure: account.smtp.secure,
                auth: {
                    user: account.user,
                    pass: account.pass
                }
            });
        
            // Message object
            let message = {
                from: '"Nodemeiler Contact" <joewan@jenosize.com>',
                to: 'jororo1234@gmail.com',
                subject: 'Nodemailer is unicode friendly ✔',
                text: 'Hello to JoeDev!',
                html: '<p><b>Hello</b> to JoeDev!</p>'
            };
        
            transporter.sendMail(message, (err, info) => {
                if (err) {
                    console.log('Error occurred. ' + err.message);
                    return process.exit(1);
                }
        
                console.log('Message sent: %s', info.messageId);
                // Preview only available when sending through an Ethereal account
                console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
                res.redirect('home');
            });
        });
     });
 }

 function hashPassword(password){
    return new Promise((resolve, reject) => {
        bcrypt.hash(password, saltRounds, function(err, hash) {
            if(hash){
                resolve(hash);
            }else{
                reject();
            }
          });
      });
 }

 function comparePassword(password,hashPassword){
    return new Promise((resolve, reject) => {
        bcrypt.compare(password, hashPassword, function(err, res) {
            if(res){
                resolve();
            }else{
                reject();
            }
          });
      });
 }