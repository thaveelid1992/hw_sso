var express = require("express");
var exphbs  = require('express-handlebars');
var helmet = require('helmet');
const passport = require("passport");
const bodyParser = require('body-parser');
const userController = require('./controllers/user');
const homeController = require('./controllers/home');
const passportConfig = require('./config/passport');
const path = require('path');
const pug = require('pug');

var app = express();
app.engine('handlebars', exphbs());
app.set('view engine', 'handlebars');
// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', '');

app.use('views',express.static(path.join(__dirname,'./views')));
app.use(passport.initialize());
app.use(passport.session());
app.use(helmet());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//////// Home /////////
app.get('/home',homeController.index);

//////// register /////////
app.post('/register',userController.saveUserRegister);
app.post('/registercompany',userController.saveUserRegisterCompany);

//////// forgot pass /////////
app.get('/forgot',userController.getForgot);
app.post('/forgot',userController.postForgot);

//////// update //////////
app.post('/updatecompany', passport.authenticate("jwt",{session:false}),userController.updateCompany);

/////// login ////////
app.post('/signup', userController.loginMiddleware,userController.signUp);

app.post('/accountverify', userController.loginMiddleware,userController.signUp);
app.get('/account', passport.authenticate("jwt",{session:false}),userController.account);

app.get("/", (req, res) => {
  res.send('Hello');
});

app.get('/auth/facebook', passport.authenticate('facebook', { scope: ['email', 'public_profile'] }));
app.get('/auth/facebook/callback',
  passport.authenticate('facebook', { successRedirect: '/profile/facebook',
                                      failureRedirect: '/facebook/error' }));                                                            
app.get('/profile/facebook', (req, res) => {
  console.log('req.info');
  res.status(200).json({status:true, data: req.user});
});
app.get("/facebook/error", (req, res) => {
  res.status(200).json({status:200, massage : 'Error Facebook'});
});

app.listen(3000, () => {
    console.log("Server listening on port 3000!");
});
